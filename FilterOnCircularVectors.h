#ifndef FILTER_ON_CIRCULAR_VECTORS_H
#define FILTER_ON_CIRCULAR_VECTORS_H

#include <DataTypes/CircularVector.h>
#include <DataTypes/Complex.h>
#include "FilterAbstract.h"

namespace FirFilter
{
//======================================================================================================================
/**
 * \brief ������ � �������� ���������� ��������������� � ������� ������ ��������
 * \tparam CoefficientsDataType ��� �������������
 * \tparam DelayLineType ��� ������� ������ � ����� ��������
 */
template <typename CoefficientsDataType, typename DelayLineType>
class FilterOnCircularVectors : public FilterAbstract<CoefficientsDataType, DelayLineType>
{
public:
    explicit FilterOnCircularVectors(uint32_t length);
    explicit FilterOnCircularVectors(const FilterOnCircularVectors &object) = default;
    FilterOnCircularVectors &operator=(const FilterOnCircularVectors &object) = default;
    virtual ~FilterOnCircularVectors() = default;

    DelayLineType Step(const DelayLineType &value) override;

    CircularVector<DelayLineType> DelayLine;
};

//-------------------------------------------------------------------------------------------------
/**
 * \brief �����������
 * \param length ���������� ������������� �������
 */
template <typename CoefficientsDataType, typename DelayLineType>
FilterOnCircularVectors<CoefficientsDataType, DelayLineType>::FilterOnCircularVectors(uint32_t length) :
    FilterAbstract<CoefficientsDataType, DelayLineType>(length),
    DelayLine(length)
{
}

//-------------------------------------------------------------------------------------------------
/**
 * \brief ��� ����������
 * \param value ���������� ���������� �������� ��� ����������
 * \return �������� �� ������ �������
 */
template <typename CoefficientsDataType, typename DelayLineType>
DelayLineType FilterOnCircularVectors<CoefficientsDataType, DelayLineType>::Step(const DelayLineType &value)
{
    DelayLineType result = DelayLineType();
    DelayLine.ShiftRight();
    DelayLine.PushLeft(value);

    for (int32_t i = 0; i < DelayLine.Length; i++)
    {
        result += DelayLine.data[i] * FilterAbstract<CoefficientsDataType, DelayLineType>::Coefficients.data[i];
    }

    return result;
}
//======================================================================================================================

/**
 * \brief ������ � �������� ���������� ��������������� � ������� ������ ��������
 * \tparam DelayLineType ��� �������������
 */
template <typename DelayLineType>
class FilterOnCircularVectors<DelayLineType, DelayLineType> :
    public FilterAbstract<DelayLineType, DelayLineType>
{
public:
    explicit FilterOnCircularVectors(uint32_t length);
    explicit FilterOnCircularVectors(const FilterOnCircularVectors &object) = default;
    FilterOnCircularVectors &operator=(const FilterOnCircularVectors &object) = default;
    virtual ~FilterOnCircularVectors() = default;

    DelayLineType Step(const DelayLineType &value) override;

    CircularVector<DelayLineType> DelayLine;
};

//-------------------------------------------------------------------------------------------------
/**
 * \brief �����������
 * \param length ���������� ������������� �������
 */
template <typename DelayLineType>
FilterOnCircularVectors<DelayLineType, DelayLineType>::FilterOnCircularVectors(uint32_t length) :
    FilterAbstract<DelayLineType, DelayLineType>(length),
    DelayLine(length)
{
}

//-------------------------------------------------------------------------------------------------
/**
 * \brief ��� ����������
 * \param value ���������� ���������� �������� ��� ����������
 * \return �������� �� ������ �������
 */
template <typename DelayLineType>
DelayLineType FilterOnCircularVectors<DelayLineType, DelayLineType>::Step(const DelayLineType &value)
{
    DelayLineType result = DelayLineType();
    DelayLine.ShiftRight();
    DelayLine.PushLeft(value);

    for (int32_t i = 0; i < DelayLine.Length; i++)
    {
        result += DelayLine.data[i] * FilterAbstract<DelayLineType, DelayLineType>::Coefficients.data[i];
    }

    return result;
}

//======================================================================================================================
/**
 * \brief ������ � �������� ���������� ��������������� ��� ������� ����������� �����
 * \tparam T ��� ������������ ����� � ���������� ��������������
 */
template <typename T>
class FilterOnCircularVectors<T, Complex<T>> :
    public FilterAbstract<T, Complex<T>>
{
public:
    explicit FilterOnCircularVectors<T, Complex<T>>(uint32_t length);
    explicit FilterOnCircularVectors<T, Complex<T>>(const FilterOnCircularVectors<T, Complex<T>> &object) = default;
    FilterOnCircularVectors<T, Complex<T>> &operator=(const FilterOnCircularVectors<T, Complex<T>> &object) = default;
    virtual ~FilterOnCircularVectors() = default;

    Complex<T> Step(const Complex<T> &value) override;

    CircularVector<Complex<T>> DelayLine;
};

//-------------------------------------------------------------------------------------------------
/**
 * \brief ����������� ��� ����������� ������� �����
 * \param length ���������� �������������
 */
template<typename T>
FilterOnCircularVectors<T, Complex<T>>::FilterOnCircularVectors(uint32_t length) :
    FilterAbstract<T, Complex<T>>(length),
    DelayLine(length)
{

}

//-------------------------------------------------------------------------------------------------
/**
 * \brief ��� ����������
 * \param value ���������� ���������� �������� ��� ����������
 * \return �������� �� ������ �������
 */
template <typename T>
Complex<T> FilterOnCircularVectors<T, Complex<T>>::Step(const Complex<T> &value)
{
    Complex<T> result = Complex<T>();
    DelayLine.ShiftRight();
    DelayLine.PushLeft(value);

    for (int32_t i = 0; i < DelayLine.Length; i++)
    {
        result += DelayLine.data[i] * FilterAbstract<T, Complex<T>>::Coefficients.data[i];
    }

    return result;
}

//======================================================================================================================
/**
 * \brief ������ � �������� ����������� ���������� ��������������� � ������������ �������
 * \tparam T ��� ����������� ����� ��� ���������� �������������� � ����� ��������
 */
template <typename T>
class FilterOnCircularVectors<Complex<T>, Complex<T>> :
    public FilterAbstract<Complex<T>, Complex<T>>
{
public:
    explicit FilterOnCircularVectors<Complex<T>, Complex<T>>(uint32_t length);
    explicit FilterOnCircularVectors<Complex<T>, Complex<T>>(const FilterOnCircularVectors<Complex<T>, Complex<T>> &object) = default;
    FilterOnCircularVectors<Complex<T>, Complex<T>> &operator=(const FilterOnCircularVectors<Complex<T>, Complex<T>> &object) = default;
    virtual ~FilterOnCircularVectors() = default;

    Complex<T> Step(const Complex<T> &value) override;

    CircularVector<Complex<T>> DelayLine;
};

//-------------------------------------------------------------------------------------------------
/**
 * \brief �����������
 * \param length ���������� ������������� ���������� ��������������
 */
template<typename T>
FilterOnCircularVectors<Complex<T>, Complex<T>>::FilterOnCircularVectors(uint32_t length) :
    FilterAbstract<Complex<T>, Complex<T>>(length),
    DelayLine(length)
{

}

//-------------------------------------------------------------------------------------------------
/**
 * \brief ��� ����������
 * \param value ���������� ���������� �������� ��� ����������
 * \return �������� �� ������ �������
 */
template <typename T>
Complex<T> FilterOnCircularVectors<Complex<T>, Complex<T>>::Step(const Complex<T> &value)
{
    Complex<T> result = Complex<T>();
    DelayLine.ShiftRight();
    DelayLine.PushLeft(value);

    for (int32_t i = 0; i < DelayLine.Length; i++)
    {
        result += DelayLine.data[i] * Complex<T>::Conj(FilterAbstract<Complex<T>, Complex<T>>::Coefficients.data[i]);
    }

    return result;
}
}
#endif /* FILTER_ON_CIRCULAR_VECTORS_H */

