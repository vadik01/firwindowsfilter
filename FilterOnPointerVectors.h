#ifndef FILTER_ON_POINTER_VECTORS_H
#define FILTER_ON_POINTER_VECTORS_H

#include <DataTypes/Complex.h>
#include <DataTypes/PointerVector.h>
#include "FilterAbstract.h"

namespace FirFilter
{
//*********************************************************************************************************************
/**
 * \brief ������ � �������� ���������� ��������������� � ������� ������ ��������
 * \tparam CoefficientsDataType ��� �������������
 * \tparam DelayLineType ��� ������� ������ � ����� ��������
 */
template <typename CoefficientsDataType, typename DelayLineType>
class FilterOnPointerVectors : public FilterAbstract<CoefficientsDataType, DelayLineType>
{
public:
    explicit FilterOnPointerVectors(uint32_t length);
    explicit FilterOnPointerVectors(const FilterOnPointerVectors &object) = default;
    FilterOnPointerVectors &operator=(const FilterOnPointerVectors &object) = default;
    virtual ~FilterOnPointerVectors() = default;

    DelayLineType Step(DelayLineType &value) override;

    PointerVector<DelayLineType> DelayLine;
};

//-------------------------------------------------------------------------------------------------
/**
 * \brief �����������
 * \param length ���������� ������������� �������
 */
template <typename CoefficientsDataType, typename DelayLineType>
FilterOnPointerVectors<CoefficientsDataType, DelayLineType>::FilterOnPointerVectors(uint32_t length) :
    FilterAbstract<CoefficientsDataType, DelayLineType>(length),
    DelayLine(length)
{
}

//-------------------------------------------------------------------------------------------------
/**
 * \brief ��� ����������
 * \param value ���������� ���������� �������� ��� ����������
 * \return �������� �� ������ �������
 */
template <typename CoefficientsDataType, typename DelayLineType>
DelayLineType FilterOnPointerVectors<CoefficientsDataType, DelayLineType>::Step(DelayLineType &value)
{
    DelayLineType result = DelayLineType();
    DelayLine.Push(value);

    for (int32_t i = 0; i < DelayLine.Length; i++)
    {
        result += DelayLine.data[i] * FilterAbstract<CoefficientsDataType, DelayLineType>::Coefficients.data[i];
    }

    return result;
}
//======================================================================================================================

/**
 * \brief ������ � �������� ���������� ��������������� � ������� ������ ��������
 * \tparam DelayLineType ��� �������������
 */
template <typename DelayLineType>
class FilterOnPointerVectors<DelayLineType, DelayLineType> :
    public FilterAbstract<DelayLineType, DelayLineType>
{
public:
    explicit FilterOnPointerVectors(uint32_t length);
    explicit FilterOnPointerVectors(const FilterOnPointerVectors &object) = default;
    FilterOnPointerVectors &operator=(const FilterOnPointerVectors &object) = default;
    virtual ~FilterOnPointerVectors() = default;

    DelayLineType Step(DelayLineType &value) override;

    PointerVector<DelayLineType> DelayLine;
};

//-------------------------------------------------------------------------------------------------
/**
 * \brief �����������
 * \param length ���������� ������������� �������
 */
template <typename DelayLineType>
FilterOnPointerVectors<DelayLineType, DelayLineType>::FilterOnPointerVectors(uint32_t length) :
    FilterAbstract<DelayLineType, DelayLineType>(length),
    DelayLine(length)
{
}

//-------------------------------------------------------------------------------------------------
/**
 * \brief ��� ����������
 * \param value ���������� ���������� �������� ��� ����������
 * \return �������� �� ������ �������
 */
template <typename DelayLineType>
DelayLineType FilterOnPointerVectors<DelayLineType, DelayLineType>::Step(DelayLineType &value)
{
    DelayLineType result = DelayLineType();
    DelayLine.Push(value);
    const int32_t index = DelayLine.Index;
    int32_t indexCoefficients = FilterAbstract<DelayLineType, DelayLineType>::Coefficients.Length - 1;

    for (int32_t i = index; i < DelayLine.Length; i++, indexCoefficients--)
    {
        result += DelayLine.data[i] * FilterAbstract<DelayLineType, DelayLineType>::Coefficients.data[indexCoefficients];
    }

    for (int32_t i = 0; i < index; i++, indexCoefficients--)
    {
        result += DelayLine.data[i] * FilterAbstract<DelayLineType, DelayLineType>::Coefficients.data[indexCoefficients];
    }

    return result;
}

//======================================================================================================================
/**
 * \brief ������ � �������� ���������� ��������������� ��� ������� ����������� �����
 * \tparam T ��� ������������ ����� � ���������� ��������������
 */
template <typename T>
class FilterOnPointerVectors<T, Complex<T>> :
    public FilterAbstract<T, Complex<T>>
{
public:
    explicit FilterOnPointerVectors<T, Complex<T>>(uint32_t length);
    explicit FilterOnPointerVectors<T, Complex<T>>(const FilterOnPointerVectors<T, Complex<T>> &object) = default;
    FilterOnPointerVectors<T, Complex<T>> &operator=(const FilterOnPointerVectors<T, Complex<T>> &object) = default;
    virtual ~FilterOnPointerVectors() = default;

    Complex<T> Step(const Complex<T> &value) override;

    PointerVector<Complex<T>> DelayLine;
};

//-------------------------------------------------------------------------------------------------
/**
 * \brief ����������� ��� ����������� ������� �����
 * \param length ���������� �������������
 */
template<typename T>
FilterOnPointerVectors<T, Complex<T>>::FilterOnPointerVectors(uint32_t length) :
    FilterAbstract<T, Complex<T>>(length),
    DelayLine(length)
{

}


//-------------------------------------------------------------------------------------------------
/**
 * \brief ��� ����������
 * \param value ���������� ���������� �������� ��� ����������
 * \return �������� �� ������ �������
 */
template <typename T>
Complex<T> FilterOnPointerVectors<T, Complex<T>>::Step(const Complex<T> &value)
{
    Complex<T> result = Complex<T>();
    DelayLine.Push(value);
    const int32_t index = DelayLine.Index;
    int32_t counterCoefficients = FilterAbstract<T, Complex<T>>::Coefficients.Length - 1;

    for (int32_t i = index; i < FilterAbstract<T, Complex<T>>::Coefficients.Length; i++, counterCoefficients--)
    {
        result += DelayLine.data[i] * FilterAbstract<T, Complex<T>>::Coefficients.data[counterCoefficients];
    }

    for (int32_t i = 0; i < index; i++, counterCoefficients--)
    {
        result += DelayLine.data[i] * FilterAbstract<T, Complex<T>>::Coefficients.data[counterCoefficients];
    }

    return result;
}

//======================================================================================================================
/**
 * \brief ������ � �������� ����������� ���������� ��������������� � ������������ �������
 * \tparam T ��� ����������� ����� ��� ���������� �������������� � ����� ��������
 */
template <typename T>
class FilterOnPointerVectors<Complex<T>, Complex<T>> :
    public FilterAbstract<Complex<T>, Complex<T>>
{
public:
    explicit FilterOnPointerVectors<Complex<T>, Complex<T>>(uint32_t length);
    explicit FilterOnPointerVectors<Complex<T>, Complex<T>>(const FilterOnPointerVectors<Complex<T>, Complex<T>> &object) = default;
    FilterOnPointerVectors<Complex<T>, Complex<T>> &operator=(const FilterOnPointerVectors<Complex<T>, Complex<T>> &object) = default;
    virtual ~FilterOnPointerVectors() = default;

    Complex<T> Step(Complex<T> &value) override;

    PointerVector<Complex<T>> DelayLine;
};

//-------------------------------------------------------------------------------------------------
/**
 * \brief �����������
 * \param length ���������� ������������� ���������� ��������������
 */
template<typename T>
FilterOnPointerVectors<Complex<T>, Complex<T>>::FilterOnPointerVectors(uint32_t length) :
    FilterAbstract<Complex<T>, Complex<T>>(length),
    DelayLine(length)
{

}

//-------------------------------------------------------------------------------------------------
/**
 * \brief ��� ����������
 * \param value ���������� ���������� �������� ��� ����������
 * \return �������� �� ������ �������
 */
template <typename T>
Complex<T> FilterOnPointerVectors<Complex<T>, Complex<T>>::Step(Complex<T> &value)
{
    Complex<T> result = Complex<T>();
    DelayLine.Push(value);
    const int32_t index = DelayLine.Index;
    int32_t counterCoefficients = FilterAbstract<Complex<T>, Complex<T>>::Coefficients.Length - 1;

    for (int32_t i = index; i < DelayLine.Length; i++, counterCoefficients--)
    {
        result += DelayLine.data[i] *
            Complex<T>::Conj(FilterAbstract<Complex<T>, Complex<T>>::Coefficients.data[counterCoefficients]);
    }

    for (int32_t i = 0; i < index; i++, counterCoefficients--)
    {
        result += DelayLine.data[i] *
            Complex<T>::Conj(FilterAbstract<Complex<T>, Complex<T>>::Coefficients.data[counterCoefficients]);
    }

    return result;
}
//*********************************************************************************************************************
}

#endif /* FILTER_ON_POINTER_VECTORS_H */
