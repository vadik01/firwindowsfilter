#ifndef FIR_FILTER_GAUSS_WINDOW_H
#define FIR_FILTER_GAUSS_WINDOW_H

#include <DataTypes/Complex.h>
#include <DataTypes/Vector.h>
#include "FilterOnPointerVectors.h"
#include "FilterOnCircularVectors.h"
#include "FilterWindows.h"

namespace FirFilter
{
    /**
     * \brief ���-������ � ����������� ����� �� ������ ������� � �����������
     * \tparam T ��� ��������� �����
     */
    template <typename T>
    class FilterGaussWindowsFast:
        public FilterOnPointerVectors<T, Complex<T>>
    {
    public:
        FilterGaussWindowsFast(uint32_t order, T alpha, T halfBand, T frequencySampling);
        FilterGaussWindowsFast(const FilterGaussWindowsFast &object) = default;
        FilterGaussWindowsFast &operator=(const FilterGaussWindowsFast &object) = default;
        ~FilterGaussWindowsFast() = default;

        T GetWindowParameter() const;
        T GetHalfBandFilter() const;
        T GetSamplingFrequencyFilter() const;
        const Vector<T> &GetWindowFilter() const;

    private:
        Vector<T> UsingWindow;
        T Alpha;
        T HalfBand;
        T Fs;
    };
}
//=====================================================================================================================
/**
 * \brief ����������� ��� ������� � ����������� �����
 * \param order ������� ������� (�� ������� ������, ��� ����� �������������)
 * \param alpha �������� ����
 * \param halfBand �������� ������ ����������� �� ������ -6 ��
 * \param frequencySampling ������� �������������
 */
template <typename T>
FirFilter::FilterGaussWindowsFast<T>::FilterGaussWindowsFast(uint32_t order, T alpha, T halfBand, T frequencySampling) :
    FilterOnPointerVectors<T, Complex<T>>(static_cast<int32_t>(order) + 1),
    UsingWindow(static_cast<int32_t>(order) + 1),
    Alpha(alpha),
    HalfBand(halfBand),
    Fs(frequencySampling)

{
    const T normalBand = halfBand / frequencySampling / static_cast<T>(2);
    for (int32_t i = 0; i < FilterOnPointerVectors<T, Complex<T>>::DelayLine.Length; i++)
    {
        FilterOnPointerVectors<T, Complex<T>>::DelayLine.data[i] = Complex<T>();
    }

    Windows::Sinc<T>(&FilterOnPointerVectors<T, Complex<T>>::Coefficients, normalBand);
    Windows::GaussWindow<T>(&UsingWindow, Alpha);

    for (int32_t i = 0; i < FilterOnPointerVectors<T, Complex<T>>::Coefficients.Length; i++)
    {
        FilterOnPointerVectors<T, Complex<T>>::Coefficients.data[i] *= UsingWindow.data[i] * normalBand;
    }
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * \brief ��������� ������������� ��������� ������������ ����
 * \return �������� ������������ ����
 */
template <typename T>
T FirFilter::FilterGaussWindowsFast<T>::GetWindowParameter() const
{
    return Alpha;
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * \brief ��������� ������������ ������ ����������� �� ������ -6 �� ��� ������� ������������ ����
 * \return �������� ������ ����������� �� ������ -6 ��
 */
template <typename T>
T FirFilter::FilterGaussWindowsFast<T>::GetHalfBandFilter() const
{
    return HalfBand;
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * \brief ��������� ������������ ������� ������������� ��� ������� ������������ ����
 * \return ������� �������������
 */
template <typename T>
T FirFilter::FilterGaussWindowsFast<T>::GetSamplingFrequencyFilter() const
{
    return Fs;
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * \brief ��������� ������������� ������������ ���� ������������ � �������
 * \return ������������ ������������ ����
 */
template <typename T>
const Vector<T> &FirFilter::FilterGaussWindowsFast<T>::GetWindowFilter() const
{
    return UsingWindow;
}

//*********************************************************************************************************************
namespace FirFilter
{
    /**
 * \brief ���-������ � ����������� ����� �� ������ ������� ����� ��������
 * \tparam T ��� ��������� �����
 */
    template <typename T>
    class FilterGaussWindows :
        public FilterOnCircularVectors<T, Complex<T>>
    {
    public:
        FilterGaussWindows(uint32_t order, T alpha, T cutoffFrequency, T frequencySampling);
        FilterGaussWindows(const FilterGaussWindows &object) = default;
        FilterGaussWindows &operator=(const FilterGaussWindows &object) = default;
        ~FilterGaussWindows() = default;

        T GetWindowParameter() const;
        T GetHalfBandFilter() const;
        T GetSamplingFrequencyFilter() const;
        const Vector<T> &GetWindowFilter() const;

    private:
        Vector<T> UsingWindow;
        T Alpha;
        T HalfBand;
        T Fs;
    };
}
//=====================================================================================================================
/**
 * \brief ����������� ��� ������� � ����������� �����
 * \param order ������� ������� (�� ������� ������, ��� ����� �������������)
 * \param alpha �������� ����
 * \param cutoffFrequency �������� ������ ����������� �� ������ -6 ��
 * \param frequencySampling ������� �������������
 */
template <typename T>
FirFilter::FilterGaussWindows<T>::FilterGaussWindows(uint32_t order, T alpha, T cutoffFrequency, T frequencySampling) :
    FilterOnCircularVectors<T, Complex<T>>(order + 1U),
    UsingWindow(static_cast<int32_t>(order) + 1),
    Alpha(alpha),
    HalfBand(cutoffFrequency),
    Fs(frequencySampling)

{
    const T normalBand = cutoffFrequency / frequencySampling * static_cast<T>(2);
    for (int32_t i = 0; i < FilterOnCircularVectors<T, Complex<T>>::DelayLine.Length; i++)
    {
        FilterOnCircularVectors<T, Complex<T>>::DelayLine.data[i] = Complex<T>();
    }

    Windows::Sinc<T>(&(FilterOnCircularVectors<T, Complex<T>>::Coefficients), normalBand);
    Windows::GaussWindow<T>(&UsingWindow, Alpha);
    T sum = T();
    for (int32_t i = 0; i < FilterOnCircularVectors<T, Complex<T>>::Coefficients.Length; i++)
    {
        FilterOnCircularVectors<T, Complex<T>>::Coefficients.data[i] *= UsingWindow.data[i];
        sum += FilterOnCircularVectors<T, Complex<T>>::Coefficients.data[i];
    }

    for (int32_t i = 0; i < FilterOnCircularVectors<T, Complex<T>>::Coefficients.Length; i++)
    {
        FilterOnCircularVectors<T, Complex<T>>::Coefficients.data[i] /= sum;
    }
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * \brief ��������� ������������� ��������� ������������ ����
 * \return �������� ������������ ����
 */
template <typename T>
T FirFilter::FilterGaussWindows<T>::GetWindowParameter() const
{
    return Alpha;
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * \brief ��������� ������������ ������ ����������� �� ������ -6 �� ��� ������� ������������ ����
 * \return �������� ������ ����������� �� ������ -6 ��
 */
template <typename T>
T FirFilter::FilterGaussWindows<T>::GetHalfBandFilter() const
{
    return HalfBand;
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * \brief ��������� ������������ ������� ������������� ��� ������� ������������ ����
 * \return ������� �������������
 */
template <typename T>
T FirFilter::FilterGaussWindows<T>::GetSamplingFrequencyFilter() const
{
    return Fs;
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * \brief ��������� ������������� ������������ ���� ������������ � �������
 * \return ������������ ������������ ����
 */
template <typename T>
const Vector<T> &FirFilter::FilterGaussWindows<T>::GetWindowFilter() const
{
    return UsingWindow;
}


#endif /* FIR_FILTER_GAUSS_WINDOW_H */

