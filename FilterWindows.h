#ifndef FILTER_WINDOWS_H
#define FILTER_WINDOWS_H

#define _USE_MATH_DEFINES
#include <cmath>
#include <cstdint>
#include <DataTypes/Vector.h>
#include <corecrt_math_defines.h>


namespace FirFilter
{

    namespace Windows
    {
        template <typename T>
        int32_t GaussWindow(T *coefficients, int32_t length, T alpha);
        template <typename T>
        int32_t GaussWindow(Vector<T> *coefficients, T alpha);

        template <typename T>
        int32_t Sinc(T *coefficients, int32_t length, T dx);
        template <typename T>
        int32_t Sinc(Vector<T> *coefficients, T dx);

    };
}


//---------------------------------------------------------------------------------------------------------------------
/**
 * \brief ������ ������������� ��� ������������ ����
 * \param coefficients ������������������ ������ ��� �������������
 * \param length ���������� ����������� �������������
 * \param alpha �������� ����
 * \return\code length\endcode - ����� �������
 * \return -1 - � ������ ������ �����
 */
template <typename T>
int32_t FirFilter::Windows::GaussWindow(T *coefficients, int32_t length, T alpha)
{
    if (length <= 0)
    {
        cprint(L"GaussWindow: error length");
        return -1;
    }
    const int32_t halfLength = length >> 1;
    const T delta = length & 0x1 ? static_cast<T>(0.0) : static_cast<T>(0.5);
    const T _2alpha2 = static_cast<T>(2) * alpha * alpha;
    const T invOrder = static_cast<T>(1) / static_cast<T>(length - 1);
    for (int32_t i = 0; i < length; i++)
    {
        const T y = static_cast<T>(-halfLength + i + delta) * invOrder;
        coefficients[i] = exp(-_2alpha2 * y * y);
    }

    return length;
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * \brief
 * \param coefficients ������������������\code Vector\endcode ��� �������������,
 *                     ����� ���������� ���������� �������������
 * \param alpha �������� ����
 * \return\code length\endcode - ����� �������
 * \return -1 - � ������ ������ �����
 */
template <typename T>
int32_t FirFilter::Windows::GaussWindow(Vector<T> *coefficients, T alpha)
{
    return GaussWindow<T>(coefficients->data, coefficients->Length, alpha);
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * \brief ������ ������������� ��� ���� ���� sin(x)/x
 * \param coefficients ������������������ ������ ��� �������������
 * \param length ���������� ����������� �������������
 * \param dx ��� ��������� �����
 * \return\code length\endcode - ����� �������
 * \return -1 - � ������ ������ �����
 */
template <typename T>
int32_t FirFilter::Windows::Sinc(T *coefficients, int32_t length, T dx)
{
    if (length <= 0)
    {
        cprint(L"Sinc: error length");
        return -1;
    }
    const int32_t halfLength = length >> 1;
    const T delta = length & 0x1 ? static_cast<T>(0.0) : static_cast<T>(0.5);
    const T c = static_cast<T>(M_PI) * dx;
    for (int32_t i = 0; i < length; i++)
    {
        const T y = static_cast<T>(-halfLength + delta + i) * c;
        if (y == 0.0)
            coefficients[i] = static_cast<T>(1);
        else
            coefficients[i] = sin(y) / y;
    }

    return length;
}

//---------------------------------------------------------------------------------------------------------------------
/**
 * \brief ������ ������������� ��� ���� ���� sin(x)/x
 * \param coefficients ������������������\code Vector\endcode ��� �������������,
 *                     ����� ���������� ���������� �������������
 * \param dx ��� ��������� �����
 * \return\code length\endcode - ����� �������
 * \return -1 - � ������ ������ �����
 */
template <typename T>
int32_t FirFilter::Windows::Sinc(Vector<T> *coefficients, T dx)
{
    return Sinc<T>(coefficients->data, coefficients->Length, dx);
}


//---------------------------------------------------------------------------------------------------------------------


#endif /* FILTER_WINDOWS_H */
