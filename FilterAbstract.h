#ifndef FILTER_H
#define FILTER_H

#include <DataTypes/Vector.h>

namespace FirFilter
{

/**
 * \brief ������
 * \details ������ �����: ������� ���������� ������������� � �������
 *          ����� ������� ���� ������������ (�������� ������, ���� ��� ���������)
 * \tparam CoefficientsType ��� ����� ��������: ����������� ������� ��� �� ���
 */
template <typename CoefficientsType, typename DelayLineType>
class FilterAbstract
{
public:
    explicit FilterAbstract(uint32_t length);
    explicit FilterAbstract(const FilterAbstract &object) = default;
    FilterAbstract &operator=(const FilterAbstract &object) = default;
    virtual ~FilterAbstract() = default;

    virtual DelayLineType Step(const DelayLineType &value) = 0;

    Vector<CoefficientsType> Coefficients;
};
//=================================================================================================
/**
 * \brief ����������� �������
 * \param length ����� ���������� �������������� � ��������
 */
template <typename CoefficientsType, typename DelayLineType>
FilterAbstract<CoefficientsType, DelayLineType>::FilterAbstract(uint32_t length) :
    Coefficients(length)
{
    for (int32_t i = 0; i < Coefficients.Length; i++)
    {
        Coefficients.data[i] = CoefficientsType();
    }
}

}
#endif /* FILTER_H */ 

